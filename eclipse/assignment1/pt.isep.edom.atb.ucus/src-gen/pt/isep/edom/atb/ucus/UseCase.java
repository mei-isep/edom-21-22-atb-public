/**
 */
package pt.isep.edom.atb.ucus;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getName <em>Name</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getIncludes <em>Includes</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getInclude <em>Include</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getExtends <em>Extends</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getExtend <em>Extend</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.UseCase#getAssociation <em>Association</em>}</li>
 * </ul>
 *
 * @see pt.isep.edom.atb.ucus.UcusPackage#getUseCase()
 * @model
 * @generated
 */
public interface UseCase extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getUseCase_Name()
	 * @model default="" dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link pt.isep.edom.atb.ucus.UseCase#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Includes</b></em>' containment reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Include}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Includes</em>' containment reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getUseCase_Includes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Include> getIncludes();

	/**
	 * Returns the value of the '<em><b>Include</b></em>' reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Include}.
	 * It is bidirectional and its opposite is '{@link pt.isep.edom.atb.ucus.Include#getAddition <em>Addition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include</em>' reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getUseCase_Include()
	 * @see pt.isep.edom.atb.ucus.Include#getAddition
	 * @model opposite="addition"
	 * @generated
	 */
	EList<Include> getInclude();

	/**
	 * Returns the value of the '<em><b>Extends</b></em>' containment reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Extend}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends</em>' containment reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getUseCase_Extends()
	 * @model containment="true"
	 * @generated
	 */
	EList<Extend> getExtends();

	/**
	 * Returns the value of the '<em><b>Extend</b></em>' reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Extend}.
	 * It is bidirectional and its opposite is '{@link pt.isep.edom.atb.ucus.Extend#getExtendedCase <em>Extended Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extend</em>' reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getUseCase_Extend()
	 * @see pt.isep.edom.atb.ucus.Extend#getExtendedCase
	 * @model opposite="extendedCase"
	 * @generated
	 */
	EList<Extend> getExtend();

	/**
	 * Returns the value of the '<em><b>Association</b></em>' reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Association}.
	 * It is bidirectional and its opposite is '{@link pt.isep.edom.atb.ucus.Association#getUsecase <em>Usecase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association</em>' reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getUseCase_Association()
	 * @see pt.isep.edom.atb.ucus.Association#getUsecase
	 * @model opposite="usecase"
	 * @generated
	 */
	EList<Association> getAssociation();

} // UseCase
