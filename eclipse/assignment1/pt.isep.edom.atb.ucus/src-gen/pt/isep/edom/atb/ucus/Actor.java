/**
 */
package pt.isep.edom.atb.ucus;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.isep.edom.atb.ucus.Actor#getName <em>Name</em>}</li>
 *   <li>{@link pt.isep.edom.atb.ucus.Actor#getAssociation <em>Association</em>}</li>
 * </ul>
 *
 * @see pt.isep.edom.atb.ucus.UcusPackage#getActor()
 * @model
 * @generated
 */
public interface Actor extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getActor_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link pt.isep.edom.atb.ucus.Actor#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Association</b></em>' reference list.
	 * The list contents are of type {@link pt.isep.edom.atb.ucus.Association}.
	 * It is bidirectional and its opposite is '{@link pt.isep.edom.atb.ucus.Association#getActor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association</em>' reference list.
	 * @see pt.isep.edom.atb.ucus.UcusPackage#getActor_Association()
	 * @see pt.isep.edom.atb.ucus.Association#getActor
	 * @model opposite="actor"
	 * @generated
	 */
	EList<Association> getAssociation();

} // Actor
